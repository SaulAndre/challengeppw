from django.shortcuts import render

from django.http import HttpResponse


def index(request):
    return render(request, "ppw/home.html")

def do(request):
    return render(request, "ppw/do.html")